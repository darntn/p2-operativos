#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdio.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h> 
#include <sys/msg.h> 

/*----------------------------------------------------------------------------
Para compilar
gcc proyecto.cpp -o proyecto -lpthread -lrt

Para ejecutar
./proyecto

*/

struct piston {
	int posicion;   // 0 => 0cm; 1 => 10cm; 2 => 20cm; 3 => 30cm
	int estado;     // 0 => detenido; 1 => movimiento; 2 => cambiando dirección; 3 => liberado (AZ5)
	int direccion;  // 0 => sin dirección (inicial); 1 => arriba; -1 => abajo
	int traslado;   // 0 => no se está moviendo; +n => posiciones moviéndose hacia arriba; -n => posiciones moviéndose hacia abajo

	piston(): posicion(0), estado(0), direccion(0), traslado(0) {}

	bool istoUp(){ return (direccion == 1 || direccion == 0)? true: false; }

	bool istoDown(){ return (direccion == -1 || direccion == 0)? true: false; }

	bool isActive(){ return (estado != 3)? true: false; }

	bool isMoving(){ return (estado != 0)? true: false; }

	int getPosition() { return posicion*10; }

	const char* getState() {
		if (estado == 0){
			return "Det"; 
		} else if (estado == 2){
			return "\033[0;33mGir\033[0m";
		} else if (estado == 3){
			return "\033[0;31mLib\033[0m";
		} else if (estado == 1 && direccion == 1 || needMoveUp()){
			return "\033[0;33mSub\033[0m";
		} else if (estado == 1 && direccion == -1 || needMoveDown()){
			return "\033[0;33mBaj\033[0m";
		} else {
			return "--";
		}
	}

	float getDeltaK(){
		if (posicion == 1){
			return 0.1;
		} else if (posicion == 2){
			return 0.4;
		} else if (posicion == 3){
			return 0.55;
		} else {
			return 0.0;
		}
	}

	float getFinalDeltaK(){
		if (posicion-traslado == 1){
			return 0.1;
		} else if (posicion-traslado == 2){
			return 0.4;
		} else if (posicion-traslado == 3){
			return 0.55;
		} else {
			return 0.0;
		}
	}

	void startMove(int n){	
		if (estado != 0){ return ; }
		if (n < 0){         // Movimiento hacia abajo			
			if (n-posicion >= -3){
				traslado = n;
			} else {
				traslado = (3-posicion)*(-1);
			}
			if (traslado != 0){
				estado = 1;	
				if (direccion == 0){
					direccion = -1;
				}				
			}			
		} else if (n > 0){  // Movimiento hacia arriba			
			if (posicion-n >= 0){
				traslado = n;
			} else {
				traslado = posicion;
			}
			if (traslado != 0){
				estado = 1;
				if (direccion == 0){
					direccion = 1;
				}
			}			
		}
	}

	void finishMove() {
		posicion -= traslado;
		traslado = 0;
		estado = 0;
	}

	void startTurnDirection(){
		if (estado != 1){
			estado = 2;
		}
	}

	void finishTurnDirection(){
		if (estado == 2){
			estado = 0;
			direccion = (direccion == 1)? -1: 1;	
		}
	}	

	bool needMoveUp(){ return (traslado > 0)? true: false; }

	bool needMoveDown(){ return (traslado < 0)? true: false; }

	int getMovements(){ return (traslado < 0)? traslado*(-1): traslado; }

	void free(){ estado = 3; }
};

struct struct_msg { 
	long mesg_type; 
	char mesg_text[10]; 
} message;

int NUM_BARRAS = 12;
int N = 6;
float MAX = 1.5, MIN = 0.5;
char alert_msg[100];
bool END = false;
sem_t mutex, timex;
piston *pistones[6];
struct sched_param schedparam;

float K = 1;							// Valor K del reactor
int T = 2; 								// Segundos que toma realizar un movimiento
int TIMER = T*3;

void *pistontask(void *params);
void *estadotask(void *params);
void *controllertask(void *params);
void *maintask(void *params);
void *timertask(void *params);
void gotoxy(int x,int y);


int main( int argc, char** argv ){		
	int i = 0;
	pthread_t t_pistones[N];
	pthread_t t_estado, t_controlador, t_main, t_timer;	
	sem_init(&mutex, 0, 1);
	sem_init(&timex, 0, 1);

	// Configuración de afinidad de CPU
	cpu_set_t cpus;
	CPU_ZERO(&cpus);
	sched_getaffinity(0, sizeof(cpu_set_t), &cpus);
	CPU_COUNT(&cpus);

	// Configuración de planificación del proceso
	schedparam.sched_priority = sched_get_priority_max(SCHED_FIFO);


	// Creación de hilos para los pares de pistones
	for (i=0; i<N; i++){
		piston *p;
		p = (piston*)malloc(sizeof(piston));
		pistones[i] = p;
		pthread_create(&t_pistones[i], NULL, pistontask, (void*)pistones[i]);
		// Asignación del tipo de planificación
		pthread_setschedparam(t_pistones[i], SCHED_FIFO, &schedparam);
	}	

	// Creación del hilo de actualización de estado
	pthread_create(&t_estado, NULL, estadotask, NULL);
	pthread_setschedparam(t_estado, SCHED_FIFO, &schedparam);
	
	// Creación del hilo principal (Recibe valores K)
	pthread_create(&t_main, NULL, maintask, NULL);
	pthread_setschedparam(t_main, SCHED_FIFO, &schedparam);

	// Creación del hilo controlador (Maneja las barras)
	pthread_create(&t_controlador, NULL, controllertask, NULL);
	pthread_setschedparam(t_controlador, SCHED_FIFO, &schedparam);

	// Creación del hilo temporizador
	pthread_create(&t_timer, NULL, timertask, NULL);
	pthread_setschedparam(t_timer, SCHED_FIFO, &schedparam);


	for (i=0; i<N; i++){
		pthread_join(t_pistones[i],NULL);
	}
	pthread_join(t_estado, NULL);
	pthread_join(t_main, NULL);
	pthread_join(t_controlador, NULL);
	pthread_join(t_timer, NULL);

	
	sem_destroy(&mutex);
	return 0;
}


void *pistontask(void *params){
	piston *p = (piston*)params;
	float dKprevio = 0;

	while (p->isActive() || !END){

		// Valida si hubo una petición de movimiento desde el maintask
		if (p->isMoving()) {

			// Penalidad por cambio de dirección del pistón
			if ((p->istoUp() && p->needMoveDown()) || (p->istoDown() && p->needMoveUp())){				
				p->startTurnDirection();
				sleep(T);
				p->finishTurnDirection();
			}

			// Demora por movimientos
			sleep(T * p->getMovements());

			dKprevio = p->getDeltaK();

			// Finalización del movimiento
			p->finishMove();


			// Esperar para actualizar K
			sem_wait(&mutex); 

			// Actualización de K
			K -= (p->getDeltaK() - dKprevio) * 2;	

			// Liberar K
			sem_post(&mutex);

		}	
		// delay
		sleep(0.5);
	}

	return NULL;
}

void *estadotask(void *params){
	
	int i=0;
	char cabecera[200] = "\033[1;36m------------------------------------ MONITOR ----------------------------------------\033[0m\nValor K: %4.2f\t\tEstado: %s\n\n";	
	system("clear");
	gotoxy(1,1);
	printf(cabecera, K, "\033[1;32mEquilibrio\033[0m\e[?25l");
	for (int i=0; i<NUM_BARRAS; i++){
		printf("\033[1;36m\t%2d\033[0m", (i+1));
	}	
	
	while (!END){		
		gotoxy(10,2);		
		printf("%4.2f", K);
		gotoxy(1,10);
		
		if (K > MAX){
			gotoxy(33,2);
			printf("\033[1;31mSuper crítico     \033[0m\tel reactor podría explotar en %2d s", TIMER);
		} else if (K < MIN){
			gotoxy(33,2);
			printf("\033[1;31mSub crítico       \033[0m\tel reactor podría apagarse en %2d s", TIMER);
		} else {
			gotoxy(33,2);
			printf("%s", "\033[1;32mRango aceptable                                            \033[0m");
			sem_wait(&timex);
			TIMER = T*3;
			sem_post(&timex);
			strcpy(alert_msg, " ");
		}	
				
		gotoxy(1,5);
		printf("\033[1;36m%s\033[0m", "Pos:");
		for (i=0; i<NUM_BARRAS; i++){			
			printf("\t%02d", pistones[i%N]->getPosition());
		}						
		printf("\n\033[1;36m%s\033[0m", "Est:");		
		for (i=0; i<NUM_BARRAS; i++){
			printf("\t%s", pistones[i%N]->getState());
		}				
		printf("\n\033[1;36m%s\033[0m", "dK:");
		for (i=0; i<NUM_BARRAS; i++){
			printf("\t%.2f", pistones[i%N]->getDeltaK());
		}
		printf("\n\n%s\n", alert_msg);
		
		// delay
		sleep(0.5);		
	}			

	return NULL;
}

void *controllertask(void *params){
	float aporte_barras = 0;
	int i = 0;
	bool flag = false;
	// Control de barras
	while (!END){
		if (K > MAX){	
			// Reducir K ---> Bajar barras		
			while (K - aporte_barras > MAX){
				for (i = 0; i<N; i++){
					// Prioridad a barras en el mismo sentido
					if (pistones[i]->istoDown()){
						if (!pistones[i]->isMoving()){
							pistones[i]->startMove(-1);
							aporte_barras += (pistones[i]->getFinalDeltaK() - pistones[i]->getDeltaK()) * 2;
							if (K - aporte_barras <= MAX){
								break;
							}
						}						
					} else {
						flag = true;
					}
				}
				if (flag){
					// Movimiento de barras en sentido opuesto
					for (i = 0; i<N; i++){
						if (pistones[i]->istoUp()){
							if (!pistones[i]->isMoving()){
								pistones[i]->startMove(-1);
								aporte_barras += (pistones[i]->getFinalDeltaK() - pistones[i]->getDeltaK()) * 2;
								if (K - aporte_barras <= MAX){
									break;
								}
							}						
						}
					}
					flag = false;
				}				
			}			
		} else if (K < MIN){
			// Aumentar K ---> Subir barras
			while (K - aporte_barras < MIN){
				for (i = 0; i<N; i++){
					if (pistones[i]->istoUp()){
						if (!pistones[i]->isMoving()){
							pistones[i]->startMove(1);
							aporte_barras += (pistones[i]->getFinalDeltaK() - pistones[i]->getDeltaK()) * 2;
							if (K - aporte_barras >= MIN){
								break;
							}
						}						
					} else {
						flag = true;
					}
				}	
				if (flag){
					for (i = 0; i<N; i++){
						if (pistones[i]->istoDown()){
							if (!pistones[i]->isMoving()){
								pistones[i]->startMove(1);
								aporte_barras += (pistones[i]->getFinalDeltaK() - pistones[i]->getDeltaK()) * 2;
								if (K - aporte_barras >= MIN){
									break;
								}
							}						
						}
					}
					flag = false;
				}
			}			
		} else {
			// NO HACER NADA xD
			aporte_barras = 0;
		}

		// delay
		sleep(0.4);
	}
	return NULL;
}

void *maintask(void *params){
	key_t key; 
	int msgid;
	float k_received = 0;
	float diff = 0;
	int i = 0;

	// ftok to generate unique key 
	key = ftok("dnca", 65); 

	// Crear message queue
	msgid = msgget(key, 0666 | IPC_CREAT); 

	// Recibo primer mensaje, indicará el tiempo T
	msgrcv(msgid, &message, sizeof(message), 1, 0); 
	int time = atoi(message.mesg_text);
	// si se recibe un valor válido se actualiza el T default
	if (time > 0){
		T = time;
		sem_wait(&timex);
		TIMER = T*3;
		sem_post(&timex);
	}

	while (!END){
		// msgrcv to receive message 
		msgrcv(msgid, &message, sizeof(message), 1, 0); 

		// Validación AZ5
		if (strcmp(message.mesg_text, "az5\n") == 0){			
			// Liberar barras
			for (i=0; i<N; i++){
				pistones[i]->free();
			}
			strcpy(alert_msg, "Las barras han sido liberadas");
			END = true;
			break;
		}

		// convert to float
		k_received = atof(message.mesg_text);	

		// Esperar para actualizar K
		sem_wait(&mutex);		

		// Actualizar K
		K = K + k_received;	
		diff = 1 - K;		
		
		// Liberar K
		sem_post(&mutex);
	}

	// to destroy the message queue 
	msgctl(msgid, IPC_RMID, NULL); 
	
	return NULL;
}

void *timertask(void *params){
	while (!END){

		sleep(1);

		sem_wait(&timex);
		if (TIMER > 0){
			TIMER--;	
		} else {
			strcpy(alert_msg, "\033[0;31mTiempo terminado! No se pudo salvar el reactor :(\033[0m");
			END = true;
		}
		sem_post(&timex);
		
	}
	return NULL;
}

void gotoxy(int x,int y){
	printf("%c[%d;%df",0x1B,y,x);
}