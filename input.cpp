#include <stdio.h> 
#include <sys/ipc.h> 
#include <sys/msg.h> 
#include <stdlib.h>
#include <string.h>

/*----------------------------------------------------------------------------
Para compilar
gcc input.cpp -o input

Para ejecutar
./input

Para accionar AZ5 ingresar
az5

*/


// structure for message queue 
struct struct_msg { 
	long mesg_type; 
	char mesg_text[10]; 
} message; 

int main() 
{ 
	key_t key; 
	int msgid; 

	// ftok to generate unique key 
	key = ftok("dnca", 65); 

	// Creación del message queue 	
	msgid = msgget(key, 0666 | IPC_CREAT); 
	message.mesg_type = 1; 

	system("clear");
	printf("\033[1;36m------------------------- Consola K -------------------------\033[0m\n\tAZ5: az5\t\tValores K válidos: float, int\n\n");

	printf("Ingrese un tiempo T (segundos que tarda un movimiento): ");
	fgets(message.mesg_text, 10, stdin);
	msgsnd(msgid, &message, sizeof(message), 0);

	system("clear");
	printf("\033[1;36m------------------------- Consola K -------------------------\033[0m\n\tAZ5: az5\t\tValores K válidos: float, int\n\n");
	while(1){
		printf("\033[1;36m>>\033[0m "); 
		fgets(message.mesg_text, 10, stdin); 

		// msgsnd to send message 
		msgsnd(msgid, &message, sizeof(message), 0); 

		if (strcmp(message.mesg_text, "az5\n") == 0){
			break;
		}

	}	

	return 0; 
} 
